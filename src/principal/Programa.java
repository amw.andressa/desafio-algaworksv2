package principal;

import entities.ContratoTrabalho;
import entities.FolhaPagamento;
import entities.Funcionario;

import java.util.Locale;
import java.util.Scanner;

public class Programa {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);

        FolhaPagamento folhaPagamento = new FolhaPagamento();
        ContratoTrabalho contratoTrabalho = new ContratoTrabalho(14.5, 18.0);
        Funcionario funcionario = new Funcionario();

        double salarioCalculado = folhaPagamento.calcularSalario(120,8, contratoTrabalho);
        System.out.printf("Salario inicial calculado: R$ %.2f", salarioCalculado);
        System.out.println(" ");
        System.out.print("Funcionario possui quantos filhos? ");
        int qtdFilhos = sc.nextInt();
        funcionario.setQuantidadeDeFilhos(qtdFilhos);
        double novosalario = contratoTrabalho.funcionarioTemFilho(qtdFilhos, salarioCalculado);
        System.out.printf("Salario atualizado: R$ %.2f", novosalario);

    }
}
