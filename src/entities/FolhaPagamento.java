package entities;

public class FolhaPagamento {

    public double calcularSalario(int quantidadeHorasNormais, int quantidadeHorasExtras, ContratoTrabalho contratoTrabalho){
        double salarioNormal = quantidadeHorasNormais * contratoTrabalho.getValorHoraNormal();
        double salarioExtras = quantidadeHorasExtras * contratoTrabalho.getValorHoraExtra();
        return salarioNormal + salarioExtras;
    }
}
