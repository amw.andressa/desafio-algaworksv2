package entities;

public class Holerite {

    private Funcionario funcionario;
    private int totalHorasNormais;
    private int totalHorasExtras;
    private double adicionalParaFilhos;


    public Funcionario getFuncionario() {
        return funcionario;
    }

    public double calcularValorTotal(ContratoTrabalho contratoTrabalho){
        totalHorasNormais *= contratoTrabalho.getValorHoraNormal();
        totalHorasExtras *= contratoTrabalho.getValorHoraExtra();
        return totalHorasNormais + totalHorasExtras;
    }

    public double funcionarioTemFilho(int quantidade, double salario){
        double adicional = quantidade * 0.1;
        double salarioAtualizado = salario * adicional;
        return salarioAtualizado += salario;
    }


    public String toString(){
        return funcionario.getNome()
                + funcionario.getQuantidadeDeFilhos()
                + String.format("%.2f", adicionalParaFilhos);
    }
}
